#!/usr/bin/env python

# --------------------------------------------------------
# Tensorflow Faster R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Xinlei Chen, based on code from Ross Girshick
# --------------------------------------------------------

"""
Demo script showing detections in sample images.

See README.md for installation instructions before running.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import _init_paths
from model.config import cfg
from model.test import im_detect
from model.nms_wrapper import nms, soft_nms

from utils.timer import Timer
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import os, cv2
import argparse

from nets.vgg16 import vgg16
from nets.resnet_v1 import resnetv1

CLASSES = ('__background__', 'alpro_mandorla_lt1', 'alpro_riso_lt1', 'alpro_soyachoco_lt1',
'barilla_5c_pennette_gr400', 'barilla_5c_spaghetti_gr400', 'barilla_bavette_gr500', 'barilla_bucatini_gr500',
'barilla_cellentani_gr500', 'barilla_farfalle_kg1', 'barilla_fusillibucaticorti_gr500', 'barilla_int_farfalle_gr500',
'barilla_int_mezzepenne_gr500', 'barilla_lasagnette_gr500', 'barilla_mezzemanicherigate_gr500', 'barilla_reginette_gr500',
'barilla_sg_tortiglioni_gr400', 'barilla_tortiglioni_gr500', 'barilla_tortiglioni_kg1', 'barilla_trofie_gr500', 'bavaria_86_extreme_ml500',
'bavaria_86_gold_ml500', 'bavaria_86_original_ml500', 'bavaria_86_red_ml500', 'burn_original_ml250',
'cocacola_zero_ml330', 'crikcrok_tubopaprika_gr100', 'esselunga_3farri_gr400', 'esselunga_chipsrigate_gr150',
'esselunga_granoduro_gr400', 'esselunga_panbrioche_gr400', 'esselungaequilibrio_bransticks_gr375', 'esselungaequilibrio_wellflakes_gr375',
'gallo_3c_bipack_gr400', 'gallo_arborio_kg1', 'gallo_basmati_gr850', 'gallo_blondversatile_kg1,2', 'gallo_int_rosso_gr500', 'iper_ace_ml200x6',
'iper_anellinimiele_gr375', 'iper_aranciarossa_ml200x6', 'iper_cornflakes_gr375', 'iper_frostedflakes_gr375', 'iper_tropicale_ml200x6',
'lays_oliooliva_gr150', 'molisana_farfallerigate_gr500', 'molisana_mezzirigatoni_gr500', 'molisana_pennerigate_gr500',
'molisana_succhietti_gr500', 'pata_e_mediterraneo_gr120', 'pata_e_spezieorientali_gr120', 'pata_grigliata_gr130', 'pata_olioevo_gr130',
'pata_rosmarino_gr150', 'pepsi_light_ml330', 'pepsi_twist_ml330', 'pingles_tortilla_original_gr160', 'pringles_original_gr165',
'pringles_paprika_gr165', 'riomare_extra_gr240', 'riomare_insfagioli_gr320', 'riomare_insmessicana_gr320', 'riomare_instonnofarro_gr320',
'riomare_leggero_gr180', 'riomare_pescatoacanna_gr240', 'riomare_puttanesca_gr320', 'riomare_tonnofagioli_gr320', 'rummo_farfalle_gr500',
'rummo_fusilli_gr500', 'rummo_rigatoni_gr500', 'sacarlo_light_gr75', 'sancarlo_grill_gr180', 'sancarlo_h_kissmybbq_gr130',
'sancarlo_rodeo_gr120', 'sancarlo_salame_gr150', 'sancarlo_vivace_gr150', 'sancarlo_zenzero_gr150', 'sanpellegrino_aranciaficodindia_ml330',
'sanpellegrino_chinottomirto_ml330', 'sanpellegrino_limonementa_ml330', 'scotti_belgioioso_kg1', 'scotti_oro_kg1', 'scotti_roma_kg1',
'specialk_classic_gr375', 'specialk_fruttasecca_gr330', 'specialk_fruttirossi_gr325', 'voiello_fusilli_gr500', 'voiello_tofarelle_gr500',
'voiello_ziti_gr500', 'skipper', 'scotti','sale','granrisparmio','tonnorio','colgate','kellogs', 'valfrutta', 'barilla', 'mano')

NETS = {'vgg16': ('vgg16_faster_rcnn_iter_70000.ckpt',),'res101': ('res101_faster_rcnn_iter_110000.ckpt',), 'res50': ('res50_faster_rcnn_iter_110000.ckpt',)}
DATASETS= {'checkout_prod': ('prod_trainval',), 'pascal_voc': ('voc_2007_trainval',),'pascal_voc_0712': ('voc_2007_trainval+voc_2012_trainval',)}

def vis_detections(im, class_name, dets, thresh=0.5):
    """Draw detected bounding boxes."""
    inds = np.where(dets[:, -1] >= thresh)[0]
    if len(inds) == 0:
        return

    im = im[:, :, (2, 1, 0)]
    fig, ax = plt.subplots(figsize=(12, 12))
    ax.imshow(im, aspect='equal')
    for i in inds:
        bbox = dets[i, :4]
        score = dets[i, -1]

        ax.add_patch(
            plt.Rectangle((bbox[0], bbox[1]),
                          bbox[2] - bbox[0],
                          bbox[3] - bbox[1], fill=False,
                          edgecolor='red', linewidth=3.5)
            )
        ax.text(bbox[0], bbox[1] - 2,
                '{:s} {:.3f}'.format(class_name, score),
                bbox=dict(facecolor='blue', alpha=0.5),
                fontsize=14, color='white')

    ax.set_title(('{} detections with '
                  'p({} | box) >= {:.1f}').format(class_name, class_name,
                                                  thresh),
                  fontsize=14)
    plt.axis('off')
    plt.tight_layout()
    plt.draw()

def demo_images(directory):
    for root, dirname, filename in os.walk(directory):
        return filename

def demo(sess, net, image_name):
    """Detect object classes in an image using pre-computed object proposals."""

    # Load the demo image
    im_file = os.path.join(cfg.DATA_DIR, 'demo', image_name)
    im = cv2.imread(im_file)

    # Detect all object classes and regress object bounds
    timer = Timer()
    timer.tic()
    scores, boxes = im_detect(sess, net, im)
    timer.toc()
    print('Detection took {:.3f}s for {:d} object proposals'.format(timer.total_time, boxes.shape[0]))

    # Visualize detections for each class
    CONF_THRESH = 0.7
    NMS_THRESH = 0.3
    for cls_ind, cls in enumerate(CLASSES[1:]):
        cls_ind += 1 # because we skipped background
        cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
        cls_scores = scores[:, cls_ind]
        dets = np.hstack((cls_boxes, cls_scores[:, np.newaxis])).astype(np.float32)
        #keep = nms(dets, NMS_THRESH)
        keep = soft_nms(dets, sigma=0.5, method=cfg.TEST.SOFT_NMS)
        dets = dets[keep, :]
        inds = np.where(dets[:, -1] >= CONF_THRESH)[0]
        score = 0.0
        if len(inds) > 0:
            # cls found in frame
            for i in inds:
                bbox  = dets[i, :4]
                score = dets[i, -1]
                left  = bbox[0]
                top   = bbox[1]
                right = bbox[2]
                bottom= bbox[3]

                # Draw a box around the object
                cv2.rectangle(im, (left,top ), (right ,bottom), (0, 0, 255), 2)
                # Draw a label
                cv2.rectangle(im, (left, int(bottom) - 35), (right, bottom), (0, 255, 0), -1)
                font = cv2.FONT_HERSHEY_DUPLEX
                cv2.putText(im, cls + ': ' + str(score), (int(left) + 6, int(bottom) - 6), font, 1.0, (255, 255, 255), 1)
        #vis_detections(im, cls, dets, thresh=CONF_THRESH)
                cv2.imwrite(image_name+"res.png", im)
                cv2.imshow("image", im)
        key = cv2.waitKey(10) & 0xFF
        if key == ord('q'):
            break
    cv2.destroyAllWindows()

def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='Tensorflow Faster R-CNN demo')
    parser.add_argument('--net', dest='demo_net', help='Network to use [vgg16 res101]',
                        choices=NETS.keys(), default='res101')
    parser.add_argument('--dataset', dest='dataset', help='Trained dataset [checkout_prod pascal_voc pascal_voc_0712]',
                        choices=DATASETS.keys(), default='pascal_voc_0712')
    parser.add_argument('--demo_dir', type=str, dest='testdir', help='directory where all demo/test images are present')
    args = parser.parse_args()

    return args

if __name__ == '__main__':
    cfg.TEST.HAS_RPN = True  # Use RPN for proposals
    args = parse_args()

    # model path
    demonet = args.demo_net
    dataset = args.dataset
    tfmodel = os.path.join('output', demonet, DATASETS[dataset][0], 'default',
                              NETS[demonet][0])
    demodir = args.testdir


    if not os.path.isfile(tfmodel + '.meta'):
        raise IOError(('{:s} not found.\nDid you download the proper networks from '
                       'our server and place them properly?').format(tfmodel + '.meta'))

    # set config
    tfconfig = tf.ConfigProto(allow_soft_placement=True)
    tfconfig.gpu_options.allow_growth=True

    # init session
    sess = tf.Session(config=tfconfig)
    # load network
    if demonet == 'vgg16':
        net = vgg16()
    elif demonet == 'res101':
        net = resnetv1(num_layers=101)
    elif demonet == 'res50':
	       net = resnetv1(num_layers=50)
    else:
        raise NotImplementedError
    net.create_architecture("TEST", 99,
                          tag='default', anchor_scales=[8, 16, 32])
    saver = tf.train.Saver()
    saver.restore(sess, tfmodel)

    print('Loaded network {:s}'.format(tfmodel))
    im_names = demo_images(demodir)
    for im_name in im_names:
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        print('Demo for data/demo/{}'.format(im_name))
        demo(sess, net, im_name)

    #plt.show()
