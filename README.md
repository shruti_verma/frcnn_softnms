# Object Detection with Faster RCNN + SOFT_NMS
> Fast RCNNN with Soft NMS
> Tensorflow Faster RCNN for Object Detection https://arxiv.org/pdf/1702.02138.pdf
> Soft-NMS https://arxiv.org/pdf/1704.04503.pdf

In object detection pipeline, the bg/fg scores are alloted to each class according to features detected in that window. But neighboring windows often have correlated scores leading to FP. So solution is to apply NMS in post-processing step. There are basically two sub-networks, regression based for detection of bounding boxes and the classification to predict class name.
In this project we aim to improve on mAP using soft-NMS instead of simple NMS.

To add soft-nms to present object detection pipeline, changes have been made to nms_wrapper.py, config and cpu_nms.pyx files.

Object detection on products dataset, using improved soft NMS method. Here changes have been accounted
on the basis of change in sigma parameter for Gaussian weighting (other is threshold parameter for linear weighting). In config file TEST.SOFT_NMS method has been set to 2, for linear weighting you have to change it to 1.
Detections below 0.001 are discarded.

### Prerequisites
Note: All code works in python 3.5+.

1. tensorflow-gpu '1.4.0-rc0'
2. please install Cython if not installed: pip install Cython
3. opencv-3.2.0
4. easydict: pip install easydict

### Getting started

1. Clone the repo: git clone https://shruti_verma@bitbucket.org/shruti_verma/frcnn_softnms.git
2. Build the Cython modules
```Shell
   cd frcnn_softnms/lib
   make clean
   make
   cd ..
```
3. Install the [Python COCO API](https://github.com/pdollar/coco). The code requires the API to access COCO dataset.
```Shell
   cd data
   git clone https://github.com/pdollar/coco.git
   cd coco/PythonAPI
   make
   cd ../../..
```
4. Download pre-trained models.
```Shell
   mkdir -p data/imagenet_weights
   cd data/imagenet_weights
   wget -v http://download.tensorflow.org/models/vgg_16_2016_08_28.tar.gz
   tar -xzvf vgg_16_2016_08_28.tar.gz
   mv vgg_16.ckpt vgg16.ckpt
   cd ../..
   ```
5. Setup data. For training on VOC 2007 or VOC 07 + 12 same procedure is to be applied.
```Shell
   cd data
   wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar
   wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar
   wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCdevkit_08-Jun-2007.tar
```
6. Extract all of these tars in one directory VOCdevkit
```Shell
   tar xvf VOCtrainval_06-Nov-2007.tar
   tar xvf VOCtest_06-Nov-2007.tar
   tar xvf VOCdevkit_08-Jun-2007.tar
```
7. Create symlinks for the PASCAL VOC 2007 dataset
```Shell
   ln -s VOCdevkit VOCdevkit2007
   cd ..
```
8. For training on Checkout products, just extract the dataset_products.zip in the ROOT/data folder.
```Shell
   cd prod_softnms
   cd data
   unpack the zip folder
   cd dataset_products
   mkdir -p results/PRO/Main
```
9. For training:
```Shell
   ./experiments/scripts/train_faster_rcnn.sh 0 checkout_prod vgg16/res50/res101
```
10. For testing:
```Shell
   ./experiments/scripts/test_faster_rcnn.sh 0 checkout_prod vgg16/res50/res101
```
### Training

1. cd prod_softnms
2. ./experiments/scripts/train_faster_rcnn.sh [GPU_ID] [DATASET] [NET]
   [example: ./experiments/scripts/train_faster_rcnn.sh 0 checkout_prod vgg16/res50/res101]
3. Tensorboard
   tensorboard --logdir=tensorboard/vgg16/prod_trainval/
   tensorboard information for train and validation is saved under:
```
tensorboard/[NET]/[DATASET]/default/
tensorboard/[NET]/[DATASET]/default_val/
```

### Testing
1. Test and evaluate
```Shell
  cd prod_softnms
  ./experiments/scripts/test_faster_rcnn.sh [GPU_ID] [DATASET] [NET]
  [example: ./experiments/scripts/train_faster_rcnn.sh 0 checkout_prod vgg16/res50/res101]
```
2. Run demo.py
```Shell
   cd prod_softnms
   python ./tools/demo.py
```

### Results
    VGG16
    Iteration: 50000
    NMS = 0.3, G = 0.5

![result](images/res0.png)

### Building

This project code depends on two repo:

1. https://github.com/bharatsingh430/soft-nms.git
2. https://github.com/endernewton/tf-faster-rcnn.git

## Licensing

'Checkout Technologies'
