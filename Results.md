## Faster CNN with Soft NMS implemented with both frameworks Tensorflow and PyTorch

1. Tensorflow : https://github.com/endernewton/tf-faster-rcnn.git
2. Pytorch: https://github.com/ruotianluo/pytorch-faster-rcnn.git

Object detection on products dataset, using improved soft NMS method. Here changes have been accounted
on the basis of change in sigma parameter for Gaussian weighting (other is threshold parameter for linear weighting). In config file TEST.SOFT_NMS method has been set to 2, for linear weighting you have to change it to 1.
Detections below 0.001 are discarded.

Queries and Status:

1. Why inference time differs. I mean when testing starts the first image takes more time and slowly it goes down, but the image size and proposals per image are same ?
2. Had issues in Pytorch during training. Memory Error. As far as I understand computational graphs, the new graph is constructed for every new iteration, after the backprop completes the older graph is discarded as values are updated. Sometimes it showed error during computing of histograms and shuts down with Memory Error. Next time it would just kill itself. :P
3. Tested multi-scale testing in tensorflow framework of faster rcnn, though this code does not support for multiple scales at once, I changed scaled one at a time, changes were a minor lower value in inference time and a little improvement in mean average precision value.
4. The pytorch version, needs lots of changes for adding soft nms.

#### Prerequisites
Note: All code works in python 3.5+
1. tensorflow-gpu '1.4.0-rc0'
2. please install Cython if not installed: pip install Cython
3. opencv-3.2.0
4. easydict: pip install easydict

### Comparisons and Results 

##Tensorflow
Change in the inference time- 0.936s goes down to 0.082s
### VGG16
#### Iteration: 50000
NMS = 0.3, G = 0.5

| Method               |Dataset  |mAP@[0.5:0.95] | 
| :-----:              |         | :---:         |
| TF-RCNN, NMS         |voc0712  | 37.7%         | 
| TF-RCNN, Soft-NMS G  |voc0712  | 38.7%         |
| TF-RCNN, NMS         |checkout | 75.3%         | 
| TF-RCNN, Soft-NMS G  |checkout | 75.9%         | 
 
#### Iteration: 70000
| Method               |Dataset  |mAP@[0.5:0.95] | 
| :-----:              |         | :---:         |
| TF-RCNN, NMS         |checkout | 75.3%         | 
| TF-RCNN, Soft-NMS G  |checkout | 75.8%         |

### RES50
Change in the inference time- 0.854s goes down to 0.101s
####Iteration: 50000
NMS = 0.3, G = 0.5

| Method               |Dataset  |mAP@[0.5:0.95] | 
| :-----:              |         | :---:         |
| TF-RCNN, NMS         |checkout | 74.9%         | 
| TF-RCNN, Soft-NMS G  |checkout | 75.3%         |

### RES101
Change in the inference time- 0.941s goes down to 0.119s
####Iteration: 50000
NMS = 0.3, G = 0.5

| Method               |Dataset  |mAP@[0.5:0.95] | 
| :-----:              |         | :---:         |
| TF-RCNN, NMS         |checkout | 74.9%         | 
| TF-RCNN, Soft-NMS G  |checkout | 75.4%         |

### PyTorch
Change in the inference time- 0.585s/0.591s goes down to 0.079s/0.077s

### VGG16
#### Iteration: 5000
NMS = 0.3

| Method               |Dataset  |mAP@0.5  | 
| :-----:              |         | :---:   |
| TF-RCNN, NMS         |checkout | 0.8270  | 
| TF-RCNN, Soft-NMS G  |checkout | -       | 
