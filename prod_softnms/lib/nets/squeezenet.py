from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import tensorflow.contrib.slim as slim
from tensorflow.contrib.slim import losses
from tensorflow.contrib.slim import arg_scope
import numpy as np
from nets.network import Network
from model.config import cfg

def fire(inputs, squeeze_depth, expand_depth, fire_id, scope=None):
    #  squeeze --> expand 1x1
    #           --> expand 3x3 --> concat
    net = squeeze(inputs, squeeze_depth, fire_id)
    net = expand(net, expand_depth, fire_id)
    return net

def squeeze(inputs, num_outputs, fire_id):
    # squeeze
    layer_name = 'fire' + str(fire_id) + '_squeeze1x1'
    #net = conv_layer(inputs, num_outputs, 1, stride=[1, 1], layer_name=layer_name)
    net = slim.conv2d(inputs, num_outputs, 1, stride=1, padding="VALID", trainable= True, scope = layer_name)
    net = slim.nn.relu(net)
    return net

def expand(inputs, num_outputs, fire_id):
    e1x1 = expand_1x1(inputs, num_outputs, fire_id)
    e3x3 = expand_3x3(inputs, num_outputs, fire_id)
    return tf.concat([e1x1, e3x3], axis= 3, name='fire' + str(fire_id) + '_concat')

def expand_1x1(inputs, num_outputs, fire_id):
    layer_name = 'fire' + str(fire_id) + '_expand1x1'
    net = slim.conv2d(inputs, num_outputs, 1, stride=1, padding="VALID", trainable= True, scope = layer_name)
    net = slim.nn.relu(net)
    #net = conv_layer(inputs, num_outputs, 1, stride=[1, 1], layer_name=layer_name)
    return net

def expand_3x3(inputs, num_outputs, fire_id):
    layer_name = 'fire' + str(fire_id) + '_expand3x3'
    net = slim.conv2d(inputs, num_outputs, 3, stride=1, padding="SAME",trainable= True, scope = layer_name)
    net = slim.nn.relu(net)
    #net = conv_layer(inputs, num_outputs, 1, stride=[1, 1], layer_name=layer_name)
    return net

class squeezeNet(Network):
    def __init__(self):
       Network.__init__(self)
       self._feat_stride = [16, ]
       self._feat_compress = [1. / float(self._feat_stride[0]), ]
       self._scope = 'squeezenet'


    def _image_to_head(self, is_training, reuse=None):
        print ("image to head called", is_training)
        net = slim.conv2d(self._image, 64, 3, stride=2, scope='conv1')
        net = tf.nn.relu(net)
        net = slim.max_pool2d(net, 3, stride=2, padding='SAME', scope="pool1")
        net = fire(net, 16, 64, fire_id=2)
        net = fire(net, 16, 64, fire_id=3)
        net = slim.max_pool2d(net, 3, stride=2, padding='SAME', scope="pool3")
        net = fire(net, 32, 128, fire_id=4)
        net = fire(net, 32, 128, fire_id=5)
        net = slim.max_pool2d(net, 3, stride=2, padding='SAME', scope="pool5")
        net = fire(net, 48, 192, fire_id=6)
        net = fire(net, 48, 192, fire_id=7)
        net = fire(net, 64, 256, fire_id=8)
        net = fire(net, 64, 256, fire_id=9)
        net = slim.dropout(net, 0.5, is_training=True, scope='drop9')
        #net = slim.conv2d(net, 1000, 1, stride=1, scope='conv10')
        #net = tf.nn.relu(net)

        self._act_summaries.append(net)
        self._layers['head'] = net

        return net

    def _head_to_tail(self, pool5, is_training, reuse=None):
        #with tf.variable_scope(self._scope, self._scope, reuse=reuse):
        net = slim.conv2d(pool5, 1000, 1, stride=1, trainable=True, scope='conv10')
        net = tf.nn.relu(net)
        fc = slim.avg_pool2d(net, 14, 14, padding='SAME', scope='pool10')
         # average pooling done by reduce_mean
        #pool5_flat = slim.flatten(pool5, scope='flatten')
        #fc1 = slim.fully_connected(pool5_flat, 1000, scope='fc1')
        fc = tf.reduce_mean(fc, axis=[1, 2])
        return fc

    def get_variables_to_restore(self, variables, var_keep_dic):
        variables_to_restore = []
        print ('variables',variables)
        for v in variables:
         # exclude the first conv layer to swap RGB to BGR
            if v.name == ('conv1/weights:0'):
                self._variables_to_fix[v.name] = v
                continue
            if v.name.split(':')[0] in var_keep_dic:
                print('Variables restored: %s' % v.name)
                variables_to_restore.append(v)
        print ('variables to restore', variables_to_restore)
        return variables_to_restore

    def fix_variables(self, sess, pretrained_model):
        print('Fix SqueezeNet layers..')
        with tf.device("/cpu:0"):
          # fix RGB to BGR
          conv1_rgb = tf.get_variable("conv1_rgb", [3, 3, 3, 64], trainable=False)
          restorer_fc = tf.train.Saver({"conv1/weights": conv1_rgb})
          restorer_fc.restore(sess, pretrained_model)

          sess.run(tf.assign(self._variables_to_fix['conv1/weights:0'],
                             tf.reverse(conv1_rgb, [2])))
