from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import tensorflow.contrib.slim as slim
from tensorflow.contrib.slim import losses
from tensorflow.contrib.slim import arg_scope
import numpy as np

'''DenseNet-BC, with bottleneck layer and compression value theta=0.5 in paper. If theta=1 number of feature maps across
transition layers remain the same'''

def densenet(inputs, num_classes=1000, reduction=0.0, growth_rate=32, num_filters=64, num_layers=4, dropout_rate= 0.0, is_training=True, reuse=None, scope=None):
    compression = 1.0 - reduction
    num_dense_blocks = len(num_layers)

    with tf.variable_scope(scope, 'densenetxxx', [inputs, num_classes], reuse=reuse) as sc:
        end_points_collection = sc.name + '_end_points'
        with slim.arg_scope([slim.batch_norm, slim.dropout], is_training=is_training), \
         slim.arg_scope([slim.conv2d, conv, convBlock, denseBlock, transBlock],
                         outputs_collections=end_points_collection), \
         slim.arg_scope([conv], dropout_rate=dropout_rate):
         net = inputs

         # initial convolution
        net = slim.conv2d(net, num_filters, 7, stride=2, scope='conv1')
        net = slim.batch_norm(net)
        net = tf.nn.relu(net)
        net = slim.max_pool2d(net, 3, stride=2, padding='SAME')

        # dense blocks followed by transition block, dense --> Trans --> dense --> Trans --> dense --> Trans, thus num_dense_blocks - 1
        for i in range(num_dense_blocks - 1):
            net, num_filters = denseBlock(net, num_layers[i], num_filters, growth_rate,
                                        scope='dense_block' + str(i+1))

            # transition_block
            net, num_filters = transBlock(net, num_filters, compression=compression,
                                             scope='transition_block' + str(i+1))

        # last dense block which will be followed by classification layer
        net, num_filters = denseBlock(net, num_layers[-1], num_filters, growth_rate,
              scope='dense_block' + str(num_dense_blocks))

        # final blocks global average pooling followed by fc6
        with tf.variable_scope('final_block', [inputs]):
            net = slim.batch_norm(net)
            net = tf.nn.relu(net)
            net = tf.reduce_mean(net, [1,2], name='global_avg_pool', keep_dims=True)

        net = slim.conv2d(net, num_classes, 1, biases_initializer=tf.zeros_initializer(), scope='logits')

        end_points = slim.utils.convert_collection_to_dict(end_points_collection)

        if num_classes is not None:
            end_points['predictions'] = slim.softmax(net, scope='predictions')

    return net, end_points

@slim.add_arg_scope
def conv(inputs, num_filters, kernel_size, stride=1, scope=None, output_collections=None):
    '''so according the architecture it mentions that each 'conv' layer corresponds to sequence
    of Batch Normalization-ReLU-Conv. Lets first add it to the scope.
    inputs: the input to the architecture.
    kernel_size: kernel size of the filters
    num_filters: number of filters to be applied during convolution with stride 1, and given kernel size
    scope: scope name to which it belongs
    output_collections: to be passed in here for retaining or checking through layers structure and data
    '''
    with tf.variable_scope(scope, 'xx', [inputs]) as sc:
        net = slim.batch_norm(inputs)
        net = slim.nn.relu(net)
        net = slim.conv2d(net, num_filters, kernel_size)
        # check for layer structure info for further reference
        net = slim.utils.collect_named_outputs(outputs_collections, sc.name, net)

    return net

@slim.add_arg_scope
def convBlock(inputs, num_filters, scope=None, output_collections=None):
    '''According to the paper:  In our experiments, we let each 1×1 convolution produce 4k feature-maps.
    Thus for the 1x1 conv you add num_filters*4. Here k denotes the growth rate.
    inputs: the input to the architecture.
    num_filters: number of filters to be applied during convolution
    scope: scope name to which it belongs
    output_collections: to be passed in here for retaining or checking through layers structure and data
    '''
    with tf.variable_scope(scope, 'conv_blockx', [inputs]) as sc:
        net = conv(inputs, num_filters*4, 1, scope='x1')
        net = conv(net, num_filters, 3, scope='x2')
        # now we need to concat
        net = tf.concat([inputs, net], axis=3)
        net = slim.utils.collect_named_outputs(outputs_collections, sc.name, net)

    return net

@slim.add_arg_scope
def transBlock(inputs, num_filters, theta=1.0, scope=None, outputs_collections=None):
    ''' Transition layer has 1x1 convolution followed by 2x2 average pool with stride 2.
    theta denotes compression factor, which is by default taken as 1.0'''

    num_filters = int(num_filters * theta)
    with tf.variable_scope(scope, 'transition_blockx', [inputs]) as sc:
        net = conv(inputs, num_filters, 1, scope='blk')
        net = slim.avg_pool2d(net, 2)
        net = slim.utils.collect_named_outputs(outputs_collections, sc.name, net)

    return net, num_filters


@slim.add_arg_scope
def denseBlock(inputs, num_layers, num_filters, growth_rate, scope=None, output_collections=None):
    ''' Ok here, we will create the dense block as given in figure. This will call the conv_block
        defined before.
        inputs: input to the dense block
        num_layers: number of layers of conv block the dense layer needs to have
        num_filters: number of filters
        scope: scope name to which it belongs
        output_collections: to be passed in here for retaining or checking through layers structure and data
        '''
    net = inputs
    for i in range(num_layers):
        branch = i + 1
        net = convBlock(net, growth_rate, scope='conv_block'+str(branch))

        if grow_num_filters:
            num_filters += growth_rate

    net = slim.utils.collect_named_outputs(outputs_collections, sc.name, net)
    return net, num_filters
