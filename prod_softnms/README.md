## Faster CNN with Soft NMS

Below are some points:

1. A major issue with NMS is that it sets the score for neighbouring detections to zero. If an object was actually
present in that overlap threshold, it would be missed and this would lead to a drop in average precision.
2. Intutively it should be assigned a very low score while if it has low overlap, it can maintain its original detection score which makes sense.
3. so when NMS sets a hard threshold to decide on which box should be kept or removed from the neighbourhood of M, Soft NMS tries to decay the classification score of detected box, which has high overlap with M, rather than removing or suppressing it.
4. Its more like detection boxes which are far away as per threshold, are not affected whereas very close are assigned greater penalty which is gradual change, kinda continuous is nature.
5. This penalty is applied as function of Gaussian weighting and other is linear weighting and scores of boxes are updated.

#### Prerequisites
Note: All code works in python 3.5+
1. tensorflow-gpu '1.4.0-rc0'
2. please install Cython if not installed: pip install Cython
3. opencv-3.2.0
4. easydict: pip install easydict

#### Data setup

Place the dataset products folder in the data directory
```Shell
   cd prod_softnms
   cd data
   unpack the zip folder
   cd dataset_products
   mkdir -p results/PRO/Main
```

Place the imagenet weights vgg16 ckpt file as mentioned in the README.md of frcnn_softnms repo

For training: ./experiments/scripts/train_faster_rcnn.sh 0 checkout_prod vgg16

For testing: ./experiments/scripts/test_faster_rcnn.sh 0 checkout_prod vgg16

#### Results
Taking in note for softnms parameter which yeilds better results is G = 0.5, method = 2. On changing the dataset split ratio to 70:30, training on res50
and res101. Below are few tested images:

![image1](../images/0.png)

![image2](../images/1.png)

![image3](../images/2.png)

#### squeezeNet

1. Converted caffe model of SqueezeNet(v1.1) https://github.com/DeepScale/SqueezeNet, to tensorflow compatible,  with Caffe-Tensorflow  https://github.com/ethereon/caffe-tensorflow.
2. To convert it to ckpt https://github.com/alesolano/npy2ckpt, this repo was used. First an equivalent converter file for squeezenet was written to convert the pretrained model to ckpt
3. Checked with the model data, by simple code of restoring few weights of layers.
4. SqueezeNet model was implemented using tensorflow slim library.
5. Keeping the anchor ratios and scales to be same, its implemented with faster rcnn architecture
6. First tried by cutting off layers on conv10 and then connecting to roi pooling layer followed by fully_connected layers but showed bad results on training with 5000 iterations.
7. Changed the model now to connect with roi pooling layer with fire9 layer after adding dropout layer of 0.5, followed by RPN, conv10, global average pooling. Still results were bad but little then better then before :P, so thought of letting train for more iterations and see what happens.
8. At 20000 iteration, it started working improving, then tested with 70000 iteration, it came up with Mean AP = 0.5812, not good but good for experimentation :D. Some test images are in the repo inside images/squeeznet_test_images. All these tests were made using Soft NMS method, G = 0.5.
9. Observing the test trends, I think needs some rechecking with litte code, have some doubt on scales ratios as well. Needs improvement and experimentation.
Will update all these steps in more detail with steps once I complete with all testing.

![image1](../images/squeezenet_test_images/test0.png)

![image2](../images/squeezenet_test_images/test2.png)

![image3](../images/squeezenet_test_images/test3.png)
